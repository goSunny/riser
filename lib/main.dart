import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_with_api/statistics/jumptypes.dart';
import 'package:flutter_with_api/statistics/currency.dart';
import 'package:flutter_with_api/statistics/freefall.dart';
import 'package:flutter_with_api/gears/mygear.dart';

enum WidgetMarker {
  currency,
  jumptypes,
  freefall,
}

enum GearWidgetMarker {
  myGear,
  riggig,
  shop,
}

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  WidgetMarker selectedWidgetMarker = WidgetMarker.currency;
  GearWidgetMarker selectwidget = GearWidgetMarker.myGear;

  TabController _tabController;

  ScrollController _hideButtonController;
  var _isVisible = true;

  @override
  void initState() {
    super.initState();

    _tabController = new TabController(length: 4, vsync: this);

    _hideButtonController = ScrollController();

    _isVisible = true;
    _hideButtonController = new ScrollController();
    _hideButtonController.addListener(() {
      if (_hideButtonController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        if (_isVisible == true) {
          /* only set when the previous state is false
             * Less widget rebuilds 
             */
          print("**** ${_isVisible} up"); //Move IO away from setState
          setState(() {
            _isVisible = false;
          });
        }
      } else {
        if (_hideButtonController.position.userScrollDirection ==
            ScrollDirection.forward) {
          if (_isVisible == false) {
            /* only set when the previous state is false
               * Less widget rebuilds 
               */
            print("**** ${_isVisible} down"); //Move IO away from setState
            setState(() {
              _isVisible = true;
            });
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done

    MediaQueryData deviceInfo = MediaQuery.of(context);
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        backgroundColor: Colors.white30,
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: Text("C Gorden-Forbes"),
          bottom: TabBar(
            unselectedLabelColor: Colors.white30,
            labelColor: Colors.white,
            tabs: [
              Tab(text: "LOGBOOK"),
              Tab(text: "STATS"),
              Tab(text: "GEAR"),
              Tab(text: "SOCIAL"),
            ],
            controller: _tabController,
          ),
        ),
        body: TabBarView(
          children: [
            _logbook(),
            _stats(),
            _gear(),
            _social(),
          ],
          controller: _tabController,
        ),
        floatingActionButton: new Visibility(
          visible: true,
          child: SpeedDial(
            animatedIcon: AnimatedIcons.add_event,
            overlayColor: Colors.white,
            closeManually: true,
            children: [
              SpeedDialChild(
                child: Icon(Icons.add),
                label: "ADD SKYDIVE",
                backgroundColor: Colors.blue,
                onTap: () => print("It works"),
              ),
              SpeedDialChild(
                  child: Icon(Icons.add),
                  label: "ADD TUNNEL",
                  onTap: () => print("It works")),
              SpeedDialChild(
                  child: Icon(Icons.add),
                  label: "ADD B.A.S.E JUMP",
                  onTap: () => print("It works")),
              SpeedDialChild(
                  child: Icon(Icons.add),
                  label: "ADD SPACE DIVE",
                  onTap: () => print("It works")),
            ],
          ),
        ),
      ),
    );
  }

  Widget _logbook() {
    return ListView.builder(
      itemCount: 1,
      controller: _hideButtonController,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          elevation: 3.0,
          child: Padding(
            padding: const EdgeInsets.only(
                left: 8.0, right: 8.0, top: 4.0, bottom: 4.0),
            child: Container(
              height: 60,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text(
                        "390",
                        style: TextStyle(color: Colors.black, fontSize: 18.0),
                      ),
                      SizedBox(
                        height: 7.0,
                      ),
                      Text(
                        "1/111996",
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        "Fun Jump",
                        style: TextStyle(color: Colors.black, fontSize: 18.0),
                      ),
                      SizedBox(
                        height: 7.0,
                      ),
                      Text(
                        "Freefly",
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        "C1896",
                        style: TextStyle(color: Colors.red, fontSize: 18.0),
                      ),
                      SizedBox(
                        height: 7.0,
                      ),
                      Text(
                        "Skydive Mossel Bay",
                        style: TextStyle(color: Colors.black, fontSize: 12.0),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _stats() {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  alignment: Alignment.center,
                  height: 50,
                  child: Container(
                    height: 30.5,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border:
                            Border.all(color: Color(0XFF1A3D7A), width: 2.0),
                        borderRadius: BorderRadius.circular(4.0)),
                    alignment: Alignment.center,
                    child: Row(
                      children: <Widget>[
                        /////////    All Container Start   //////////
                        Container(
                          height: 29,
                          color: (selectedWidgetMarker == WidgetMarker.currency)
                              ? Color(0XFF1A3D7A)
                              : Theme.of(context).primaryColor,
                          child: FlatButton(
                            onPressed: () {
                              //_getDivData(false);
                              setState(() {
                                selectedWidgetMarker = WidgetMarker.currency;
                              });
                            },
                            child: Text(
                              "CURRENCY",
                              textDirection: TextDirection.ltr,
                              style: TextStyle(
                                color: (selectedWidgetMarker ==
                                        WidgetMarker.currency)
                                    ? Color(0XFFFFFFFF)
                                    : Color(0XFF1A3D7A),
                                fontSize: 16.0,
                                decoration: TextDecoration.none,
                                fontFamily: 'Roboto',
                                fontWeight: (selectedWidgetMarker ==
                                        WidgetMarker.currency)
                                    ? FontWeight.bold
                                    : FontWeight.normal,
                              ),
                            ),
                          ),
                        ),
                        ////////////  AllContainer End ///////////////

                        ////////////  Income Container Start  //////////
                        Container(
                          height: 29,
                          color:
                              (selectedWidgetMarker == WidgetMarker.jumptypes)
                                  ? Color(0XFF1A3D7A)
                                  : Theme.of(context).primaryColor,
                          child: FlatButton(
                            onPressed: () {
                              // _getDivData(true);
                              setState(() {
                                selectedWidgetMarker = WidgetMarker.jumptypes;
                              });
                            },
                            child: Text(
                              "JUMP TYEPS",
                              textDirection: TextDirection.ltr,
                              style: TextStyle(
                                color: (selectedWidgetMarker ==
                                        WidgetMarker.jumptypes)
                                    ? Color(0XFFFFFFFF)
                                    : Color(0XFF1A3D7A),
                                fontSize: 16.0,
                                decoration: TextDecoration.none,
                                fontFamily: 'Roboto',
                                fontWeight: (selectedWidgetMarker ==
                                        WidgetMarker.jumptypes)
                                    ? FontWeight.bold
                                    : FontWeight.normal,
                              ),
                            ),
                          ),
                        ),
                        ////////////  Income Container End  //////////

                        //////  Expense Container Start   ///////
                        Container(
                          height: 29,
                          color: (selectedWidgetMarker == WidgetMarker.freefall)
                              ? Color(0XFF1A3D7A)
                              : Theme.of(context).primaryColor,
                          child: FlatButton(
                            onPressed: () {
                              //_getDivData(false);
                              setState(() {
                                selectedWidgetMarker = WidgetMarker.freefall;
                              });
                            },
                            child: Text(
                              "FREEFALL",
                              textDirection: TextDirection.ltr,
                              style: TextStyle(
                                color: (selectedWidgetMarker ==
                                        WidgetMarker.freefall)
                                    ? Color(0XFFFFFFFF)
                                    : Color(0XFF1A3D7A),
                                fontSize: 16.0,
                                decoration: TextDecoration.none,
                                fontFamily: 'Roboto',
                                fontWeight: (selectedWidgetMarker ==
                                        WidgetMarker.freefall)
                                    ? FontWeight.bold
                                    : FontWeight.normal,
                              ),
                            ),
                          ),
                        ),
                        ////////////  Expense Container End  //////////
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: 600,
              child: getCustomContainer(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _gear() {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  alignment: Alignment.center,
                  height: 30,
                  child: Container(
                    height: 30,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border:
                            Border.all(color: Color(0XFF1A3D7A), width: 0.0),
                        borderRadius: BorderRadius.circular(4.0)),
                    alignment: Alignment.center,
                    child: Row(
                      children: <Widget>[
                        /////////    All Container Start   //////////
                       
                        Container(
                          height: 20,
                          
                             
                          child: GestureDetector(
                            onTap: () {
                              //_getDivData(false);
                              setState(() {
                                selectwidget = GearWidgetMarker.riggig;
                              });
                            },
                            child: Text(
                              "Rigging",
                              textDirection: TextDirection.rtl,
                              style: TextStyle(
                                color: (selectwidget ==
                                        GearWidgetMarker.riggig)
                                    ? Color(0XFFE6E6E6)
                                    : Color(0XFF1A3D7A),
                                fontSize: 16.0,
                                decoration: TextDecoration.none,
                                fontFamily: 'Roboto',
                                fontWeight: (selectwidget ==
                                        GearWidgetMarker.riggig)
                                    ? FontWeight.bold
                                    : FontWeight.normal,
                              ),
                            ),
                          ),
                        ),

                         SizedBox(
                          width: 30.0,
                        ),
                        ////////////  AllContainer End ///////////////

                        ////////////  Income Container Start  //////////
                        Container(
                          height: 29,
                          color:
                              (selectwidget == GearWidgetMarker.myGear)
                                  ? Color(0XFF1A3D7A)
                                  : Theme.of(context).primaryColor,
                          child: FlatButton(
                            onPressed: () {
                              // _getDivData(true);
                              setState(() {
                                selectwidget = GearWidgetMarker.myGear;
                              });
                            },
                            child: Text(
                              "My Gear",
                              textDirection: TextDirection.ltr,
                              style: TextStyle(
                                color: (selectwidget ==
                                       GearWidgetMarker.myGear)
                                    ? Color(0XFFFFFFFF)
                                    : Color(0XFF1A3D7A),
                                fontSize: 16.0,
                                decoration: TextDecoration.none,
                                fontFamily: 'Roboto',
                                fontWeight: (selectwidget ==
                                        GearWidgetMarker.myGear)
                                    ? FontWeight.bold
                                    : FontWeight.normal,
                              ),
                            ),
                          ),
                        ),
                        ////////////  Income Container End  //////////

                        //////  Expense Container Start   ///////
                        Container(
                          height: 29,
                          color: (selectwidget == GearWidgetMarker.shop)
                              ? Color(0XFF1A3D7A)
                              : Theme.of(context).primaryColor,
                          child: FlatButton(
                            onPressed: () {
                              //_getDivData(false);
                              setState(() {
                                selectwidget= GearWidgetMarker.shop;
                              });
                            },
                            child: Text(
                              "Shop",
                              textDirection: TextDirection.ltr,
                              style: TextStyle(
                                color: (selectwidget ==
                                        GearWidgetMarker.shop)
                                    ? Color(0XFFFFFFFF)
                                    : Color(0XFF1A3D7A),
                                fontSize: 16.0,
                                decoration: TextDecoration.none,
                                fontFamily: 'Roboto',
                                fontWeight: (selectedWidgetMarker ==
                                        WidgetMarker.freefall)
                                    ? FontWeight.bold
                                    : FontWeight.normal,
                              ),
                            ),
                          ),
                        ),
                        ////////////  Expense Container End  //////////
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: 200,
              child: getListView(),
            ),
          ],
        ),
      ),
    );
  }

  getGearWidget() {
    return Container(
      child: mygear(),
    );
  }

  Widget _social() {
    return jumptypes();
  }

  Widget getCustomContainer() {
    switch (selectedWidgetMarker) {
      case WidgetMarker.currency:
        return getCurrencyWidget();
      case WidgetMarker.jumptypes:
        return getJumptypesWidget();
      case WidgetMarker.freefall:
        return getFreefallWidget();
    }
    return getCurrencyWidget();
  }

  Widget getListView() {
    switch (selectwidget) {
      case GearWidgetMarker.myGear:
        return getGearWidget();
      case GearWidgetMarker.riggig:
        return getRigWidget();
      case GearWidgetMarker.shop:
        return getShopWidget();
    }
    return getGearWidget();
  }

  Widget getCurrencyWidget() {
    return Container(
      child: currency(),
    );
  }

  Widget getJumptypesWidget() {
    return Container(
      child: jumptypes(),
    );
  }

  Widget getFreefallWidget() {
    return Container(
      child: freefall(),
    );
  }

  Widget getRigWidget() {
    return Container(
      child: Text("Rigging Widget"),
    );
  }

  Widget getShopWidget() {
    return Container(
      child: Text("Shop widget"),
    );
  }
}
