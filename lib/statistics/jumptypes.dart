import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;


class jumptypes extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    
    return _Jumps();
  }

}

class _Jumps extends State<jumptypes>{

  List<charts.Series<Task, String>> _seriesPieData;

  _generateData() {
    var piedata = [
      new Task('Coaching', 58, Color(0xff3366cc)),
      new Task('Training', 36, Color(0xff990099)),
      new Task('FunJump', 293, Color(0xff109618)),
      new Task('Competetion', 15, Color(0xfffdbe19)),
      new Task('Sleep', 19, Color(0xffff9900)),
    ];

    _seriesPieData.add(
      charts.Series(
        data: piedata,
        domainFn: (Task task, _) => task.task,
        measureFn: (Task task, _) => task.taskvalue,
        colorFn: (Task task, _) =>
            charts.ColorUtil.fromDartColor(task.colorval),
        id: 'jumptypes',
        labelAccessorFn: (Task row, _) => '${row.taskvalue}',
      ),
    );
  }

   @override
  void initState() {
    super.initState();
    _seriesPieData = List<charts.Series<Task, String>>();

    _generateData();

  }
  
  @override
  Widget build(BuildContext context) {
    
    return Container(
      
      color: Colors.white30,
      
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Container(
          
          margin: EdgeInsets.only(top:13.0),
          child: Center(
            child: Column(
              children: <Widget>[
                Text(
                  'JUMP TYPES',
                  style: TextStyle(color: Colors.black, fontSize: 24.0, fontWeight: FontWeight.bold, fontFamily: "Roboto"),
                ),
            
                Expanded(
                  child: charts.PieChart(_seriesPieData,
                      animate: true,
                      animationDuration: Duration(seconds: 1),
                      behaviors: [
                        new charts.DatumLegend(
                          outsideJustification:
                              charts.OutsideJustification.middleDrawArea,
                          horizontalFirst: false,
                          desiredMaxRows:5,
                          cellPadding:
                              new EdgeInsets.only(right: 4.0, bottom: 4.0),
                          entryTextStyle: charts.TextStyleSpec(
                              color: charts.MaterialPalette.purple.shadeDefault,
                              fontFamily: 'Georgia',
                              fontSize: 16),
                        )
                      ],
                      defaultRenderer: new charts.ArcRendererConfig(
                          arcWidth:220,
                          arcRendererDecorators: [
                            new charts.ArcLabelDecorator(
                                labelPosition: charts.ArcLabelPosition.inside)
                          ])),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}

class Task {
  String task;
  int taskvalue;
  Color colorval;

  Task(this.task, this.taskvalue, this.colorval);
}

