import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;


class currency extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    
    return _Currency();
  }

}

class _Currency extends State<currency>{

List<charts.Series<currencyValue, int>> _seriesLineData;
_generateData() {
      var linesalesdata = [
      new currencyValue(0, 150),
      new currencyValue(1, 212),
      new currencyValue(2, 156),
      new currencyValue(3, 142),
      new currencyValue(4, 412),
      
    ];
  


    _seriesLineData.add(
      charts.Series(
        colorFn: (__, _) => charts.ColorUtil.fromDartColor(Color(0xFF00B8F0)),
        id: 'currency',
        data: linesalesdata,
        domainFn: (currencyValue c, _) => c.year,
        measureFn: (currencyValue c, _) => c.amount,
      ),
    );
   

    
    
  }


@override
  void initState() {
    super.initState();
    _seriesLineData = List<charts.Series<currencyValue, int>>();
    _generateData();
  }

  @override
  Widget build(BuildContext context) {
    
    return  Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: Container(
                  height: 200,
                  color: Colors.white,
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                            'CURRENCY',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                        Expanded(
                          child: charts.LineChart(
                            
                            _seriesLineData,
                            defaultRenderer: new charts.LineRendererConfig(
                                includeArea: true, stacked: true),
                            animate: true,
                            animationDuration: Duration(seconds: 1),
                            behaviors: [
         new charts.ChartTitle('Years',
            behaviorPosition: charts.BehaviorPosition.bottom,
            titleOutsideJustification:charts.OutsideJustification.middleDrawArea),
     
       
      ],
      
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
          
      
  }

}

class currencyValue{

  int year;
  int amount;
  currencyValue(this.year, this.amount);

}