import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;


class freefall extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    
    return _Freefall();
  }

}

class _Freefall extends State<freefall>{

List<charts.Series<currencyValue, int>> _seriesLineData;
_generateData() {
      var freefalldata = [
      new currencyValue(1, 150),
      new currencyValue(2, 212),
      new currencyValue(3, 156),
      new currencyValue(4, 142),
      new currencyValue(5, 412),
      
    ];

      var freefalldata1 = [
      new currencyValue(1, 20),
      new currencyValue(2, 15),
      new currencyValue(3, 150),
      new currencyValue(4, 148),
      new currencyValue(5, 436),
      
    ];
  


    _seriesLineData.add(
      charts.Series(
        colorFn: (__, _) => charts.ColorUtil.fromDartColor(Color(0xff990099)),
        id: '109mph',
        data: freefalldata,
        domainFn: (currencyValue c, _) => c.year,
        measureFn: (currencyValue c, _) => c.amount,
      ),
    );

    _seriesLineData.add(
      charts.Series(
        colorFn: (__, _) => charts.ColorUtil.fromDartColor(Color(0xff990099)),
        id: '147mph',
        data: freefalldata1,
        domainFn: (currencyValue c, _) => c.year,
        measureFn: (currencyValue c, _) => c.amount,
      ),
    );

    
    
  }


@override
  void initState() {
    super.initState();
    _seriesLineData = List<charts.Series<currencyValue, int>>();
    _generateData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  height: 200,
                  color: Colors.white,
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                            'FREEFALL',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                        Expanded(
                          child: charts.LineChart(
                            
                            _seriesLineData,
                            defaultRenderer: new charts.LineRendererConfig(
                                includeArea: true, stacked: true),
                            animate: true,
                            animationDuration: Duration(seconds: 1),
                            behaviors: [
         new charts.ChartTitle('109mph',
            behaviorPosition: charts.BehaviorPosition.start,
            titleOutsideJustification:charts.OutsideJustification.middleDrawArea),
      new charts.ChartTitle('147mph',
            behaviorPosition: charts.BehaviorPosition.bottom,
            titleOutsideJustification:charts.OutsideJustification.middleDrawArea),
      
       
      ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
          
      
  }

}

class currencyValue{

  int year;
  int amount;
  currencyValue(this.year, this.amount);

}